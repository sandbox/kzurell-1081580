<?php

// $Id$

/**
 * @file
 * Install, update and uninstall functions for the Candidate Questionnaires module.
 */

/**
 * Implementation of hook_install().
 */
function cq_install() {

  //pathauto
  //variable_set('pathauto_cq_supportsfeeds', NULL);
  //variable_set('pathauto_cq_main_pattern', 'users/[user-raw]/candidate_questions');
  //variable_set('pathauto_cq_edit_pattern', 'users/[user-raw]/edit_candidate_questions');
  //variable_set('pathauto_cq_bulkupdate', TRUE);

  variable_set('cq_title', t('Candidate Questionnaires'));

  variable_set('cq_default_terms', t("
Candidates must be nominated/self-nominated and must accept the nomination to respond to a questionnaire. Accepting nomination means you can respond to the questionnaire described above, and your name as it appears in your account will appear publicly in lists of respondents for this questionnaire.

Questionnaires are published at the discretion of the administrator. Automatic publication date and time are approximate and are subject to change.

Once the questionnaire is published, you will no longer be able to respond to questions or change previous responses. If you do not respond to all questions before publication, the system may indicate your incomplete or absent responses.
"));

  //clear out conflicting content.
  //db_query("UPDATE {node} SET type = 'page' WHERE type = 'candidate_questions'");

  $result = drupal_install_schema('cq');

  drupal_set_message(t('Candidate Questions installed.'));
}

/**
 * Implementation of hook_schema().
 *
 * Questionnaires are nodes.
 * Questions belong to one or more questionnaires.
 * Responses belong to a questionnaire and a question.
 * Nominations are either name/email or uid
 */
function cq_schema() {
  $schema['cq_questionnaires'] = array(
      'description' => t('Additional information for questionnaire nodes.'),
      'fields' => array(
          'nid' => array(
              'description' => t('Node ID of the questionnaire.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
          ),
          'uid' => array(
              'description' => t('User ID of the questionnaire author.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
          ),
          'cq_before' => array(
              'description' => t('Public description before publication.'),
              'type' => 'varchar',
              'length' => 65535,
          ),
          'cq_after' => array(
              'description' => t('Public description after publication.'),
              'type' => 'varchar',
              'length' => 65535,
          ),
          'cq_preview' => array(
              'description' => t('Allow preview questions during embargo.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
              'default' => 0,
          ),
          'cq_state' => array(
              'description' => t('Publication state of the questionnaire.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => FALSE,
              'default' => 0, //CQ_NONE
          ),
          'cq_pubdate' => array(
              'description' => t('Date to publish questionnaire, or NULL if not to autopublish.'),
              'default' => NULL,
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => FALSE,
          ),
          'cq_published' => array(
              'description' => t('Date when was actually published, or NULL if not yet published.'),
              'default' => NULL,
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => FALSE,
          ),
          'cq_terms' => array(
              'description' => t('Terms of use text, that candidates must agree to.'),
              'default' => NULL,
              'type' => 'text',
              'not null' => TRUE,
          ),
      ),
      'primary key' => array('nid'),
  );

  $schema['cq_questions'] = array(
      'description' => t('Questions for questionnaires.'),
      'fields' => array(
          'nid' => array(
              'description' => t('Node ID of the question.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
          ),
          'cq_qrid' => array(
              'description' => t('Node ID of the questionnaire that uses this question.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
          ),
          'cq_sid' => array(
              'description' => t('Serial number (Question 1 - n) for this question in its questionnaire.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
          ),
      ),
      'primary key' => array('nid'),
  );

  $schema['cq_responses'] = array(
      'description' => t('Responses to questionnaires.'),
      'fields' => array(
          'nid' => array(
              'description' => t('Node ID of the response.'),
              'type' => 'serial',
              'unsigned' => TRUE,
              'not null' => TRUE,
          ),
          'cq_qrid' => array(
              'description' => t('Questionnaire this response belongs to.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
          ),
          'cq_qid' => array(
              'description' => t('Question this response responds to.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
          ),
          'cq_sid' => array(
              'description' => t('Serial for this response in its questionnaire.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
          ),
          'uid' => array(
              'description' => t('User ID of the responding user.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
          ),
          'cq_updated' => array(
              'description' => t('Date of last update.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => FALSE,
          ),
      ),
      'primary key' => array('nid'),
  );

  $schema['cq_nominations'] = array(
      'description' => t('Candidates nominated to respond to questionnaires.'),
      'fields' => array(
          'nid' => array(
              'description' => t('Node ID of the questionnaire the candidate is nominated for.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
          ),
          'name' => array(
              'description' => t('Name of candidate nominated.'),
              'type' => 'varchar',
              'length' => '255', //required
              'not null' => FALSE,
              'default' => NULL,
          ),
          'email' => array(
              'description' => t('Email of candidate nominated.'),
              'type' => 'varchar',
              'length' => '255', //required
              'not null' => FALSE,
              'default' => NULL,
          ),
          'uid' => array(
              'description' => t('User ID of candidate nominated.'),
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => FALSE,
              'default' => NULL,
          ),
          'email_sent' => array(
              'description' => t('Email sent to candidate inviting acceptance.'),
              'type' => 'int',
              'size' => 'tiny',
              'default' => 0,
              'unsigned' => TRUE,
              'not null' => FALSE,
          ),
          'accepted' => array(
              'description' => t('Candidate has accepted nomination.'),
              'type' => 'int',
              'size' => 'tiny',
              'default' => 0,
              'unsigned' => TRUE,
              'not null' => FALSE,
          ),
      ),
      'primary key' => array(),
  );

  return $schema;
}

/**
 * Implementation of hook_uninstall().
 */
function cq_uninstall() {

  variable_del('cq_title');
  variable_del('cq_default_terms');

  $result = drupal_uninstall_schema('cq');

  drupal_set_message(t('Candidate Questions uninstalled.'));
}