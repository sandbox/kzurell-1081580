<?php

// $Id$

/**
 * @file
 * Question node type.
 *
 * Questions are nodes with additional information from the cq_questions table.
 */

/**
 * Implementation of hook_perm(). Not currently used, stub.
 * @return An array of permissions
 */
function cq_question_perm() {
  return array(
  );
}

/**
 * Implementation of hook_access() for question nodes.
 * @param $op The access operation.
 * @param $node The object node.
 * @param $account The account desiring access.
 * @return TRUE or FALSE.
 */
function cq_question_access($op, $node, $account) {

  $canques = user_access('create questionnaires', $account);

  if ($op == 'view')
    return user_access("view questionnaires", $account);

  $own = FALSE;
  if (is_object($node))
    if (is_int($node->uid))
      $own = ($node->uid == $account->uid);

  switch ($op) {
    case 'create':
      return $canques;
    case "update":
      return $own && $canques;
    case "delete":
      return $own && user_access('delete questionnaires', $account);
  }

  return FALSE;
}

/**
 * Implementation of hook_insert() for questions.
 * @param $node The node to insert into cq_questions.
 */
function cq_question_insert($node) {
  db_query("INSERT INTO {cq_questions} (nid, cq_qrid, cq_sid) VALUES (%d, %d, %d)",
          $node->nid, $node->cq_qrid, $node->cq_sid);
}

/**
 * Implementation of hook_update() for questions.
 * @param $node The node to update in cq_questions.
 */
function cq_question_update($node) {
  // If this is a new node or we're adding a new revision,
  /* if ($node->revision) {
    candidate_questions_insert($node);
    }
    else { */
  db_query("UPDATE {cq_questions} SET cq_sid = %d WHERE nid = %d",
          $node->cq_sid, $node->nid);
  /* } */
}

/**
 * Implementation of hook_delete() for questions.
 * @param $node The node to delete from cq_questions.
 */
function cq_question_delete($node) {
  //Delete node-attached data.
  db_query('DELETE FROM {cq_questions} WHERE nid = %d', $node->nid);
}

/**
 * Load additional data for a question node.
 *
 * @param $node
 *   Node to expand with additional data.
 * @return An object with properties to be added to the Question node.
 */
function cq_question_load($node) {
  $additions = db_fetch_object(db_query('SELECT * FROM {cq_questions} WHERE nid = %d', $node->nid));
  return $additions;
}

/**
 * Load a node and verify that it is a question.
 *
 * @param $nid Node number to load.
 * @return A Question node object.
 */
function cq_load_question($nid = NULL) {

  $node = node_load($nid);

  if (!$node) {
    drupal_set_message(t('Internal error: the specified URL is invalid (question)'), 'error', FALSE);
    drupal_goto('<front>');
  }

  $type = node_get_types('type', 'cq_question');

  if (!($type->type == $node->type)) {
    drupal_set_message(t('Internal error: the specified URL does not refer to a question'), 'error', FALSE);
    drupal_goto('<front>');
  }


  return $node;
}

/**
 * View a question node
 *
 * @param $node
 *   A Question node to view.
 * @param $teaser
 *   Return just the teaser or the complete node.
 * @param $page
 * @return
 *   The node with viewable content in $node['content'].
 */
function cq_question_view($node, $teaser = FALSE, $page = FALSE) {

  global $user;

  $node = node_prepare($node, $teaser);


  $qrnode = cq_load_questionnaire($node->cq_qrid);

  //Breadcrumb, display the enclosing questionnaire title.
  drupal_set_title(t('Question !sid', array('!sid' => $node->cq_sid)));
  $breadcrumb[] = l(t('Home'), '<front>');
  $breadcrumb[] = l($qrnode->title, "node/{$qrnode->nid}");
  drupal_set_breadcrumb($breadcrumb);


  //Who am I?
  $self = $user->uid == $qrnode->uid;
  $administer = $self || user_access('create questionnaires');
  $respond = user_access('respond to questionnaires');


  //Load responses to questionnaire, get count.
  $responses = array();
  $users = array();
  $results = db_query('SELECT nid, uid FROM {cq_responses} WHERE cq_qid = %d', $node->nid);
  while ($result = db_fetch_array($results)) {
    $responses[$result['nid']] = cq_load_response($result['nid']);
    $users[$result['uid']] = NULL; //list of users
  }

  //Cache users named in the responses to this question.
  foreach ($users as $index => $nothing) {
    $users[$index] = user_load($index);
  }

  //Random order for respondents.
  shuffle($responses);

  $respentries = array();
  foreach ($responses as $rid => $resp) {
    $nuser = $users[$resp->uid];

    $respentries[$nuser->uid . '-' . $rid] = array(
        '#weight' => 1,
        '#value' => theme('cq_response', $resp, $nuser),
    );
  }

  //Check if user has accepted nomination to enclosing questionnaire?

  $respcnt = count($respentries);
  $respcnt = array(
      '#value' => theme('cq_misc', format_plural($respcnt, '1 response', "@count responses"), "cq-respcnt"),
      '#weight' => 5,
  );
  $node->content['count'] = $respcnt;



  /* If displaying the teaser, display question and report on responses. */
  if ($teaser) {
    return $node;
  }



  switch ($qrnode->cq_state) {

    case CQ_NONE:
      drupal_set_message(t('Questionnaire has not yet been opened for responses.'), 'status', FALSE);
      break;

    case CQ_PUBLISHED: //Published: do not allow response edits?
      // Display responses.
      $node->content = array_merge($node->content, $respentries);
      break; //CONDITIONAL ON WHETHER RESPONDENTS CAN EDIT RESPONSES AFTER PUBLICATION

    case CQ_EMBARGOED:

      drupal_set_message(t('Questionnaire has not yet been published.'), 'status', FALSE);
      // Display responses for moderators/admins.
      if ($administer) {
        $node->content = array_merge($node->content, $respentries);
      }

      break;
  }

  return $node;
}

/**
 * Display question editing form.
 *
 * This is not the candidate's response form.
 *
 * @param $node
 *   Question node to be edited.
 * @param $form_state
 *   Form state.
 * @return string
 *   Form array for editing questions.
 */
function cq_question_form(&$node, $form_state) {

  global $user;

  //ensure we know what questionnaire
  if (property_exists($node, 'cq_qrid')) {
    //existing question, should have qr
    $qr = $node->cq_qrid;
  }
  else {
    //new question, we should know qr and q
    $qr = arg(3);
    if (!($qr)) {
      //stub
      drupal_set_message('To add a question, select a questionnaire.');
      $u = $user->uid;
      if ($u)
        drupal_goto("user/{$u}");
      else {
        //stub
        drupal_goto('<front>');
      }
    }
  }



  $type = node_get_types('type', $node);

  if ($type->has_title) {
    $form['title'] = array(
        '#type' => 'textfield',
        '#title' => check_plain($type->title_label),
        '#required' => TRUE,
        '#default_value' => $node->title,
        '#weight' => -5
    );
  }

  $form['heading'] = array(
      '#type' => 'item',
      '#title' => t('Question !ques', array('!ques' => $node->cq_sid)),
  );

  if ($type->has_body) {
    $form['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);
  }

  $form['cq_qrid'] = array(
      '#type' => 'value',
      '#value' => $qr,
  );
  $form['cq_sid'] = array(
      '#type' => 'value',
      '#value' => @$node->cq_sid,
  );

  return $form;
}

