<?php

// $Id$

/**
 * @file
 * The Questionnaire node type for Candidate Questionnaire.
 *
 * Questionnaires are nodes with added information from database table
 * cq_questionnaires.
 *
 */

/**
 * Implementation of hook_access() for questionnaires.
 * @param $op The operation to permit or refuse.
 * @param $node The node to permit or refuse.
 * @param $account The user to permit or refuse.
 * @return TRUE to permit, FALSE to refuse.
 */
function cq_questionnaire_access($op, $node, $account) {

  $canques = user_access('create questionnaires', $account);

  if ($op == 'view')
    return user_access("view questionnaires", $account);

  $own = FALSE;
  if (is_object($node))
    if (is_int($node->uid))
      $own = ($node->uid == $account->uid);

  switch ($op) {
    case 'create':
      return $canques;
    case "update":
      return $own && $canques;
    case "delete":
      return $own && user_access('delete questionnaires', $account);
  }

  return FALSE;
}

/**
 * Implementation of hook_insert() for questionnaires.
 * @param $node The node to insert into cq_questionnaires.
 */
function cq_questionnaire_insert($node) {
  db_query("INSERT INTO {cq_questionnaires} (nid, cq_pubdate, cq_before, cq_after, cq_preview, cq_state, cq_terms, uid) VALUES (%d, %d, '%s', '%s', %d, %d, '%s', %d)",
          $node->nid, $node->cq_pubdate, $node->cq_before, $node->cq_after, $node->cq_preview, $node->cq_state, $node->cq_terms, $node->uid);
  //Kludge to store a NULL.
  if (is_null($node->cq_pubdate))
    db_query("UPDATE {cq_questionnaires} SET cq_pubdate = NULL WHERE nid = %d", $node->nid);
}

/**
 * Implementation of hook_update() for questionnaires.
 * @param $node The node to update in cq_questionnaires.
 */
function cq_questionnaire_update($node) {
  // if this is a new node or we're adding a new revision,
  /* if ($node->revision) {
    candidate_questions_insert($node);
    }
    else { */
  db_query("UPDATE {cq_questionnaires} SET cq_pubdate = %d, cq_published = %d, cq_before = '%s', cq_after = '%s', cq_preview = %d, cq_state = %d, cq_terms = '%s', uid = %d WHERE nid = %d",
          $node->cq_pubdate, $node->cq_published, $node->cq_before, $node->cq_after, $node->cq_preview, $node->cq_state, $node->cq_terms, $node->uid, $node->nid);
  /* } */
  //Kludge to store nulls.
  if (is_null($node->cq_pubdate))
    db_query("UPDATE {cq_questionnaires} SET cq_pubate = NULL WHERE nid = %d", $node->nid);
  if (is_null($node->cq_published))
    db_query("UPDATE {cq_questionnaires} SET cq_published = NULL WHERE nid = %d", $node->nid);
}

/**
 * Implementation of hook_delete() for questionnaires.
 * @param $node Node to delete from cq_questionnaires.
 */
function cq_questionnaire_delete($node) {
  //Delete node-attached data.
  db_query('DELETE FROM {cq_questionnaires} WHERE nid = %d', $node->nid);
  //Delete questions?
  //db_query('DELETE FROM {cq_questions} WHERE nid = %d', $node->nid);
  //Delete responses?
  //db_query('DELETE FROM {cq_responses} WHERE nid = %d', $node->nid);
}

/**
 * Load additional data for a questionnaire node.
 *
 * @param $node
 *   Node to expand with additional data.
 * @return
 *   An object with properties to add to a Questionnaire node.
 */
function cq_questionnaire_load($node) {
  $additions = db_fetch_object(db_query('SELECT * FROM {cq_questionnaires} WHERE nid = %d', $node->nid));
  return $additions;
}

/**
 * Load a node and verify that it is a questionnaire.
 *
 * @param $nid Node number to load
 * @return A questionnaire node.
 */
function cq_load_questionnaire($nid = NULL) {

  $node = node_load($nid);

  if (!$node) {
    drupal_set_message(t('Internal error: the specified URL is invalid (questionnaire)'), 'error', FALSE);
    drupal_goto('<front>'); // Has caused some eternal redirect problems.
  }

  $type = node_get_types('type', 'cq_questionnaire');

  if (!($type->type == $node->type)) {
    drupal_set_message(t('Internal error: the specified URL does not refer to a questionnaire'), 'error', FALSE);
    drupal_goto('<front>');
  }


  return $node;
}

/**
 * View a questionnaire node.
 *
 * @param $node
 *   Questionnaire node to view.
 * @param $teaser
 *   Return just the teaser or the complete node
 * @param $page
 * @return
 *   The node with viewable content in $node['content']
 */
function cq_questionnaire_view($node, $teaser = FALSE, $page = FALSE) {

  global $user;

  //Breadcrumb.
  $breadcrumb[] = l(t('Home'), '<front>');
  drupal_set_breadcrumb($breadcrumb);


  //We're viewing a node, so get ready.
  $node = node_prepare($node, $teaser);

  //Who are we talking to?
  $own = ($user->uid == $node->uid);

  $admin = user_access('create questionnaires');
  $compose = $admin && ($node->cq_state == CQ_NONE);

  $respond = user_access('respond to questionnaires');

  $accepted = db_result(db_query('SELECT accepted FROM {cq_nominations} WHERE nid = %d AND uid = %d',
                          $node->nid, $user->uid));



  //What are we doing?
  $embargoed = $node->cq_state == CQ_EMBARGOED;
  $preview = !($teaser || $page);

  // Important dates
  $pubdate = !is_null($node->cq_pubdate) ? format_date($node->cq_pubdate) : NULL;
  $published = !is_null($node->cq_published) ? format_date($node->cq_published) : NULL;
  $pubdateopen = !is_null($node->cq_pubdate) ? format_date($node->cq_pubdate) : t('(open)');
  $publishedny = !is_null($node->cq_published) ? format_date($node->cq_published) : t('(none yet)');


  //---------------------------------
  //Body for questionnaires is only ever meant for admin/respondents only.
  //Everyone else sees "before" or "after".
  $node->content['body']['#type'] = 'item';
  $node->content['body']['#title'] = t('Instructions for candidates');
  if (!($admin || $respond)) {
    unset($node->content['body']);
  }


  /* Set up report, options. */
  switch ($node->cq_state) {
    case CQ_NONE:
      $msg = t('This questionnaire is not yet opened for responses.');
      $mlink = l('Embargo this questionnaire and open for responses now', "node/{$node->nid}/embargo");
      break;
    case CQ_EMBARGOED:
      if (!is_null($pubdate))
        $msg = t('This questionnaire will be published on !date.',
                        array('!date' => $pubdate));
      else
        $msg = t('This questionnaire will be published at a later date.');
      $mlink = l('Publish questionnaire now', "node/{$node->nid}/publish") . t(' or ')
              . l('Close questionnaire to responses now', "node/{$node->nid}/unembargo") . t('.');
      break;

    case CQ_PUBLISHED:
      $msg = t('This questionnaire was published on !date',
                      array('!date' => $published));
      $mlink = l('Remove questionnaire from publication now', "node/{$node->nid}/unpublish");
      break;
  }


  // Display report to viewer.
  $node->content['report'] = array(
      '#weight' => 1,
      '#value' => theme('cq_rawnotice', $msg),
  );


  // If we're only showing the teaser, that's all for now.
  if ($teaser)
    return $node;
  //-----------------------------------
  // Display questionnaire moderator information.
  // Report on auto-publish settings, even if not set.
  if ($admin && $own) {

    // Prevent notices during new node _preview_.
    if (!$preview)
      drupal_set_message(t('Administrators: !link', array('!link' => $mlink)), 'warning', FALSE);


    // Display publication parameters.
    $pubn = t('This questionnaire scheduled publication date is !pubd.<br/>Final publication date was !pubdd.',
                    array('!pubd' => $pubdateopen, '!pubdd' => $publishedny));
    $node->content['autopubreport'] = array(
        '#type' => 'item',
        '#title' => t('Publication'),
        '#value' => theme('cq_misc', $pubn, 'cq-pubn'),
        '#description' => t('Edit the questionnaire to change these settings.'),
    );



    /* List nominations. */
    $header = array('Name', 'Email', 'Emailed', 'Accepted');
    $rows = array();
    $results = db_query("SELECT * FROM {cq_nominations} WHERE nid = %d", $node->nid);
    while ($result = db_fetch_array($results)) {
      unset($row);
      $ulink = l($result['name'], "user/{$result['uid']}");
      $row[] = ($result['uid'] != 0) ? $ulink : check_plain($result['name']);
      $row[] = check_plain($result['email']);
      $row[] = $result['email_sent'] ? t('Yes') : t('No');
      $row[] = $result['accepted'] ? t('Yes') : t('No');
      $rows[] = $row;
    }

    if (count($rows)) {
      $noms = theme('table', $header, $rows, array('width' => '100%'));
    }
    else {
      $noms = theme('cq_misc', t('No candidates nominated'), 'cq-emph');
    }

    $noml = ($node->cq_state != CQ_PUBLISHED) ? l(t('Nominate a candidate'), "node/{$node->nid}/nominate") : t('');
    $node->content['noms'] = array(
        '#weight' => 1,
        '#type' => 'item',
        '#title' => t('Nominations'),
        '#value' => $noms,
        '#description' => t('These are candidates nominated for the questionnaire. !noml', array('!noml' => $noml)),
    );

    $node->content['terms'] = array(
        '#weight' => 2,
        '#type' => 'item',
        '#title' => t('Terms of service'),
        '#value' => check_plain($node->cq_terms),
        '#collapsable' => TRUE,
        '#collapsed' => TRUE,
        '#description' => t('These are terms that candidates must agree to in order to respond to the questionnaire.'),
    );
  }






  switch ($node->cq_state) {
    case CQ_EMBARGOED:

      // Display 'before' message for all viewers.
      $node->content['cqbefore'] = array(
          '#weight' => 2,
          '#type' => 'item',
          '#title' => t('Introduction'),
          '#value' => check_plain($node->cq_before),
              //'#description' => t('Description'),
      );

      // If eligible, offer to receive self-nomination.
      if ($respond && !$accepted) {
        $selflink = l(t('Self-nominate'), "node/{$node->nid}/self-nominate");
        drupal_set_message(t('Candidates: do you need to respond to this questionnaire? !link', array('!link' => $selflink)));
      }

      break;

    case CQ_PUBLISHED:

      // Display 'after' message for all viewers.
      $node->content['cqafter'] = array(
          '#weight' => 2,
          '#type' => 'item',
          '#title' => t('Introduction'),
          '#value' => check_plain($node->cq_after),
      );

      break;

    case CQ_NONE:
      if (!$own)
      //SHORT CIRCUIT
        return $node;
  }




  // TODO: Wizard-type lead through questions.

  /* List responses. */
  // For moderators, link to questions.
  // For candidates, link to add response form or existing response node.
  // For viewers, ...

  $questions = array();
  $responses = array();
  $users = array();

  //Load questions.
  $results = db_query('SELECT nid FROM {cq_questions} WHERE cq_qrid = %d ORDER BY cq_sid', $node->nid);
  while ($result = db_fetch_array($results)) {
    $questions[$result['nid']] = node_load($result['nid']);
  }

  // Load responses.
  // Not sure how to code this, used
  //   $uidclause = $respond ? " uid = %d " : ' TRUE ';
  // before.
  if ($respond && $embargoed)
    $results = db_query("SELECT nid, cq_qid, uid FROM {cq_responses} WHERE cq_qrid = %d AND uid = %d", $node->nid, $user->uid);
  else
    $results = db_query("SELECT nid, cq_qid, uid FROM {cq_responses} WHERE cq_qrid = %d", $node->nid);

  while ($result = db_fetch_array($results)) {
    $rnid = $result['nid'];
    $rqid = $result['cq_qid'];
    $ruid = $result['uid'];
    //Use references?
    $responses[$rqid][$ruid] = node_load($rnid);
    $users[$ruid] = NULL; //Union list of users.
  }

  // Cache users.
  foreach ($users as $index => $nothing) {
    $users[$index] = user_load($index);
  }


  // Reproduce questions.
  $rows = array();
  $ruid = $user->uid;

  foreach ($questions as $qid => $question) {
    unset($row);
    $row[] = $question->cq_sid;

    //Default: link to question node.
    $work = l($question->body, "node/{$question->nid}");
    $note = t('');

    // If responses are possible...
    if ($embargoed)
    // If user is responding, generate link to existing responses or create new response.
      if ($respond && $accepted) {
        if (!array_key_exists($qid, $responses)) {
          // Create link to new response.
          $work = l($question->body, "node/add/cq-response/{$question->cq_qrid}/{$question->nid}");
          $note = t('');
        }
        else {
          // Create link to existing response.
          $rnode = $responses[$qid][$ruid]->nid;
          $work = l($question->body, "node/{$rnode}");
          $note = t('Yes');
        }
      }

    //Create row.
    $row[] = $work;
    $row[] = $note;

    $rows[] = $row;
    //array('#value' => theme('new_response', $qid, $aid, $user->uid, $user->name, $resp['text']),
  }


  //Display results.
  if (count($rows)) {

    $header = array(t('Number'), t('Question'), $respond ? t('Started?') : t(''));
    $node->content["questions"] = array(
        '#type' => 'item',
        '#title' => t('Questions'),
        '#description' => t('Click the question to create or edit a response.'),
        '#value' => theme('table', $header, $rows, array('width' => '100%')),
        '#weight' => 3,
    );
  }
  else {
    $qlink = $compose ? l('Add a question', "node/{$node->nid}/questions") : '';
    $node->content['nope'] = array(
        '#weight' => 3,
        '#type' => 'item',
        '#value' => t('This questionnaire has no questions. !qlink', array('!qlink' => $qlink)),
        '#title' => t('No questions'),
    );
  }

  return $node;
}

/**
 * Display questionnaire editing form.
 *
 * This is not the candidate's response form.
 *
 * @param $node
 *   Questionnaire node to be edited.
 * @param $form_state
 *   Form state.
 * @return string
 *   Form array for editing Questionnaires.
 */
function cq_questionnaire_form(&$node, $form_state) {

  /*$form['advisory'] = array(
      '#weight' => '-10',
      '#type' => 'markup',
      '#value' => theme('cq_misc', t('Fill in these blanks and click <em>Save</em>. You can then enter questions.'), 'cq-emph'),
  );*/


  $type = node_get_types('type', $node);

  if ($type->has_title) {
    $form['title'] = array(
        '#type' => 'textfield',
        '#title' => check_plain($type->title_label),
        '#default_value' => $node->title,
        '#weight' => -5
    );
  }

  if ($type->has_body) {
    $form['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);
  }


  /* Questions not listed here yet; tried it, didn't work well. */


  // Now we define the form elements specific to our node type.
  $form['puboptions'] = array(
      '#tree' => FALSE,
      '#type' => 'fieldset',
      '#title' => t('Publication Options'),
      '#description' => t('These options control publication of the questionnaire.'),
  );

  $form['puboptions']['doautopub'] = array(
      '#type' => 'checkbox',
      '#default_value' => (@$node->cq_pubdate != NULL),
      '#title' => t('Automatically publish'),
      '#description' => t('Automatically publish at the date below.')
  );
  $form['puboptions']['cq_preview'] = array(
      '#type' => 'checkbox',
      '#default_value' => 0,
      '#title' => t('Preview Questions'),
      '#description' => t('Preview questions during embargo period.')
  );

  $deftime = time() + 31 * 24 * 60 * 60;
  $tmppubdate = is_null(@$node->cq_pubdate) ? $deftime : $node->cq_pubdate;

  //Actual 'cq_pubdate' value.
  $form['puboptions']['cq_pubdate'] = array(
      '#type' => 'value',
      '#value' => $tmppubdate,
  );

  //Form elements to receive new 'cq_pubdate' value.
  $fields = getdate($tmppubdate);
  $datefield = array(
      'month' => $fields['mon'],
      'day' => $fields['mday'],
      'year' => $fields['year']
  );
  $hourfield = $fields['hours'];

  $form['puboptions']['_cq_pubdate'] = array(
      '#type' => 'date',
      '#default_value' => $datefield,
      '#title' => t('Publication date'),
      '#description' => t('Date to automatically publish this questionnaire.')
  );

  $hours = drupal_map_assoc(range(0, 23));
  //Convert to imperial time for now.
  foreach ($hours as $key => $value) {
    $m = $value < 12 ? 'am' : 'pm';
    $h = $value > 12 ? $value - 12 : $value;
    $h = ($h == 0) ? 12 : $h; //Fix '0am'.
    $hours[$key] = $h . ":00" . $m;
  }

  $form['puboptions']['_cq_pubhour'] = array(
      '#type' => 'select',
      '#options' => $hours,
      '#value' => $hourfield,
      '#title' => t('Publication time'),
      '#description' => t('Hour to publish this questionnaire.'),
  );


  if (@$node->cq_published) {
    $pubmsg = t('Questionnaire was published on !date.', array('!date' => format_date($node->cq_published)));
  }
  else {
    $pubmsg = t('Questionnaire is not published.');
  }
  $form['puboptions']['published'] = array(
      '#type' => 'markup',
      '#value' => theme('cq_misc', $pubmsg, 'cq_emph'),
  );
  $form['puboptions']['cq_published'] = array(
      '#type' => 'value',
      '#value' => @$node->cq_published,
  );

  // Terms of service, customizable.
  $terms = @$node->cq_terms != '' ? $node->cq_terms : variable_get('cq_default_terms', '');
  $form['cq_terms'] = array(
      '#type' => 'textarea',
      '#default_value' => $terms,
      '#title' => t('Terms of service'),
      '#description' => t('These are the terms of service that candidates must agree to in order to respond to a questionnaire.'),
  );


  $form['cq_before'] = array(
      '#type' => 'textarea',
      '#default_value' => @$node->cq_before,
      '#title' => t('Description before publication'),
      '#description' => t('Introduces candidate questionnaire before the publication date.'),
          //'#attributes' => array('class' => 'teaser-processed'),
  );

  $form['cq_after'] = array(
      '#type' => 'textarea',
      '#default_value' => @$node->cq_after,
      '#title' => t('Description after publication'),
      '#description' => t('Introduces candidate questionnaire after the publication date.'),
          //'#attributes' => array('class' => 'teaser-processed'),
  );

  $form['cq_state'] = array(
      '#type' => 'value',
      '#value' => empty($node->cq_state) ? CQ_NONE : $node->cq_state,
  );


  $form['#submit'][] = "cq_questionnaire_form_submit";

  return $form;
}

/**
 * Fix up questionnaire node, perform cq_question changes.
 * @param $form
 * @param $form_state
 */
function cq_questionnaire_form_submit($form, &$form_state) {

  global $user;

  //convert date & hour to timestamp
  $date = $form_state['values']['_cq_pubdate'];
  $hour = $form_state['values']['_cq_pubhour'];
  $value = mktime($hour, 0, 0, $date['month'], $date['day'], $date['year']); //is_dst?
  if (!$value) {
    $value = @$form_state['values']['cq_pubdate']; //No change, so there.
  }

  if ($form_state['values']['doautopub'] == 0)
    $value = NULL;
  $form_state['values']['cq_pubdate'] = $value;
}

/* Try to fix rare multiple "Split summary at cursor" problem. Not appearing lately.
  function cq_questionnaire_form_alter(&$form, $form_state, $form_id) {
  if($form_id=='cq_questionnaire_node_form') {
  $form['body_field']['teaser_include']['#access'] = FALSE;
  }
  }
 */


/* Question administration through questionnaire. Each question is also a node
 * that can be worked with as usual.
 */

/**
 * Display a draggable list of existing questions for this questionnaire, and a form for
 * quick entry of new questions.
 * @param $form_state Form state.
 * @param $nid Node ID of the enclosing questionnaire.
 * @return A form array for this form.
 */
function cq_question_question_form(&$form_state, $nid) {

  $node = cq_load_questionnaire($nid);
  drupal_set_title(check_plain($node->title));

  // If questionnaire is embargoed, must prevent changes.
  if ($node->cq_state == CQ_EMBARGOED) {
    drupal_set_message(t('No changes to questions or question order are permitted during embargo. To make changes, first close questionnaire to responses.'), 'error');
    drupal_goto("node/{$node->nid}");
  }



  $form = array();

  // Re-display questionnaire body, instructions for moderators.
  $form['qrbody'] = array(
      '#type' => 'markup',
      '#weight' => '-9',
      '#value' => theme('cq_questionnaire_body', $node),
  );

  //Load questions.
  $questions = array();
  $results = db_query("SELECT nid FROM {cq_questions} WHERE cq_qrid = %d ORDER BY cq_sid", $node->nid);
  while ($qnid = db_result($results)) {
    $qnode = node_load($qnid); //cq_load_question()? We know them, so necessary?
    if (!$qnode)
      continue;
    $questions[] = $qnode;
  }

  //Display existing questions as a draggable table.
  $form['items'] = array(
      '#tree' => TRUE,
  );
  foreach ($questions as $ignore => $qnode) {

    $form['items'][$qnode->nid] = array(
        'title' => array(
            '#type' => 'markup',
            '#value' => theme('cq_question_item', $qnode),
        ),
        'weight' => array(
            '#type' => 'weight',
            '#delta' => count($questions),
            '#default_value' => $qnode->cq_sid,
        ),
        'nid' => array(
            '#type' => 'hidden',
            '#value' => $qnode->nid,
        ),
    );
  }

  //Append form for new questions.
  $form['new_question'] = array(
      '#type' => 'textarea',
      '#title' => t('New Question'),
      '#description' => t('Enter a new question, then press "Save changes" below to record.'),
  );

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save changes',
  );

  //Remember which questionnaire this is for.
  $form['qrid'] = array(
      '#type' => 'value',
      '#value' => $node->nid,
  );

  return $form;
}

//function cq_question_question_form_validate($form, &$form_state) {

/**
 * Submit handler for cq_question_question_form().
 * @global $user The current user.
 * @param $form Form data.
 * @param $form_state Processing state of the form.
 */
function cq_question_question_form_submit($form, &$form_state) {

  global $user;

  //Record updated weights.
  $newweights = array();
  if (array_key_exists('items', $form_state['values']))
    foreach ($form_state['values']['items'] as $item) {
      $newweights[$item['nid']] = $item['weight'];
    }
  asort($newweights, SORT_NUMERIC);
  $sid = 1;
  foreach ($newweights as $qnid => $ignore) {
    $node = node_load($qnid); //SQL UPDATE?
    $node->cq_sid = $sid;
    node_save($node);
    $sid++;
  }


  //Add new question if present.
  if ($form_state['values']['new_question'] != '') {

    $type = node_get_types('type', 'cq_question', FALSE);
    $node = array(
        'title' => t('Question !sid', array('!sid' => $sid)),
        'body' => $form_state['values']['new_question'],
        'uid' => $user->uid,
        'cq_qrid' => $form_state['values']['qrid'],
        'cq_sid' => $sid,
        'type' => $type->type,
        'language' => ''
            //Default node publish?
    );
    $node = (object) $node;

    node_save($node);

    drupal_set_message(t('New question created.'));
  }
}

/* Nominations */

/**
 * Displays a form for moderator-issued nomintations for a particular
 * questionnaire.
 * @param $form_state Form state.
 * @param $nid The node ID of the target questionnaire.
 * @return A form array for the nomination form.
 */
function cq_questionnaire_nominations_form(&$form_state, $nid) {

  //Display known candidates and existing nominations.
  $candrole = variable_get('cq_candidaterole', CQ_DEFAULTROLE); //KLUDGE!!
  $userarray = array();
  $alreadyarray = array();
  $results = db_query('SELECT uid FROM {users_roles} WHERE rid = %d', $candrole);
  while ($u = db_result($results)) {
    $uu = user_load($u);
    $userarray[$u] = $uu->name;
  }

  if (count($userarray)) {
    $desc = t('Check candidates already registered to nominate them for this questionnaire.');
    $results = db_query('SELECT uid FROM {cq_nominations} WHERE nid = %d', $nid);
    while ($u = db_result($results))
      $alreadyarray[$u] = $u;
  }
  else {
    $desc = t('No candidates known to system.');
  }

  $form['existing'] = array(
      '#title' => t('Existing candidates'),
      '#description' => $desc,
      '#type' => 'checkboxes',
      '#options' => $userarray,
      '#default_value' => $alreadyarray,
  );

  $form['description'] = array(
      '#type' => 'item',
      '#title' => t('Instructions'),
      '#value' => t("To nominate a candidate to respond to this questionnaire, enter their name and email address below. ")
      . t("Use the most common form of their name (spelling is important) and a campaign-specific email address."),
      '#description' => t('Note: multiple nominations have no effect'),
  );

  $form['name'] = array(
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => t('Name'),
      '#description' => t("Candidate's name"),
  );

  $form['email'] = array(
      '#type' => 'textfield',
      '#title' => t('Email'),
      '#description' => t("Candidate's email address"),
      '#required' => FALSE,
  );

  $form['nid'] = array(
      '#type' => 'value',
      '#value' => $nid,
  );

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Nominate/Update'),
  );

  return $form;
}

/**
 * Validate handler for cq_questionnaire_nominations_form().
 * @param $form Form data.
 * @param $form_state Form state.
 */
function cq_questionnaire_nominations_form_validate($form, &$form_state) {

  //Ensure node is a questionnaire.
  $nid = $form_state['values']['nid'];

  $node = cq_load_questionnaire($nid);

  $name = @$form_state['values']['name'];
  $email = @$form_state['values']['email'];
  if (($name xor $email)) {
    form_set_error('name', t('Must specify a name with an email address.'));
  }
  if ($email) {
    if (!valid_email_address($email)) {
      form_set_error('email', t('The email address is not well-formed.'));
    }
  }
}

/**
 * Submit handler for cq_questionnaire_nominations_form().
 * @param $form Form data.
 * @param $form_state Form state.
 */
function cq_questionnaire_nominations_form_submit($form, &$form_state) {

  $values = $form_state['values'];
  $nid = $values['nid'];
  $existing = $values['existing'];
  $name = $values['name'];
  $email = @$values['email']; //TODO: strip +xxx -xxx
  //Check for pre-existing nominations.
  $components = split('[[:punct:][:space:]]+', $name);
  $namepattern = join('%', $components);

  foreach ($existing as $uid) {
    $u = user_load($uid);

    db_query('INSERT INTO {cq_nominations} (nid, name, email, uid, email_sent, accepted) VALUES (%d, "%s", "%s", %d, 0, 0)',
            $nid, $u->name, $u->mail, $uid);
  }

  if ($name && $email) {
    $results = db_query('SELECT COUNT(nid) FROM {cq_nominations} WHERE nid = %d AND ( email = "%s" OR name LIKE "%s")',
                    $nid, $email, $namepattern);
    if (db_result($results) == 0) {
      db_query('INSERT INTO {cq_nominations} (nid, name, email, email_sent) VALUES (%d, "%s", "%s", 0)',
              $nid, $name, $email);
      //Send email?
    }
  }

  drupal_set_message(t('Nominations have been recorded.'));

  drupal_goto("node/{$nid}");
}

/* Self-nomination. */

/**
 * Generates a form for user self-nomination for a questionnaire.
 * @param $form_state Form state.
 * @param $nid The questionnaire node to nominate for.
 * @return A form array for self-nomination.
 */
function cq_questionnaire_selfnomination_form(&$form_state, $nid) {
  global $user;

  $node = cq_load_questionnaire($nid);
  $admin = user_load($node->uid);

  if ($node->cq_pubdate) {
    $pubdate = format_date($node->cq_pubdate);
    $cqdescription = t('Expected to be published on !date.', array('!date' => $pubdate));
  }
  else {
    $cqdescription = t('No scheduled publication date.');
  }

  $form['cqdescription'] = array(
      '#type' => 'item',
      '#title' => t('Questionnaire %title', array('%title' => $node->title)),
      '#value' => check_plain($node->body),
      '#description' => $cqdescription,
  );
  $form['nid'] = array(
      '#type' => 'value',
      '#value' => $nid,
  );

  $terms = check_plain($node->cq_terms);
  $form['advisory'] = array(
      '#type' => 'item',
      '#title' => t('Terms and Conditions'),
      '#value' => $terms,
  );

  $question = t('Do you wish to accept nomination for "%qrtext"?', array('%qrtext' => $node->title));
  $adminlink = l($admin->name, "user/{$admin->uid}");
  $description = t('For more information about this questionnaire, contact !adminlink.',
                  array('!adminlink' => $adminlink));
  $cancelpath = "node/{$node->nid}";

  return confirm_form($form, $question, $cancelpath, $description, t('Accept'), t('Cancel'));
}

/**
 * Submit handler for cq_questionnaire_selfnomination_form().
 * @global $user The current user.
 * @param $form Form data.
 * @param $form_state Form state.
 */
function cq_questionnaire_selfnomination_form_submit($form, &$form_state) {
  global $user;

  $nid = $form_state['values']['nid'];

  /* INSERT ... ON DUPLICATE KEY UPDATE */

  $already = db_result(db_query('SELECT count(nid) FROM {cq_nominations} WHERE nid = %d and uid = %d',
                          $form_state['values']['nid'], $user->uid));
  if (!$already) {
    $result = db_query('INSERT INTO {cq_nominations} (nid, name, email, uid, email_sent, accepted) VALUES (%d, "%s", "%s", %d, %d, %d)',
                    $nid, $user->name, $user->mail, $user->uid, 1, 1);
    drupal_set_message(t('Your self-nomination/acceptance has been recorded.'));
  }
  else {
    $result = db_query('UPDATE {cq_nominations} SET accepted = 1, email_sent = 1 WHERE nid = %d AND uid = %d', $nid, $user->uid);
    drupal_set_message('Your nomination acceptance has been recorded.');
  }

  drupal_goto("node/{$nid}");
}

/* Embargo: Open questionnaires for responses from candidates, hide responses
 * from other candidates and general public.
 */

/**
 * Generates a form for embargo of questionnaires.
 * @param $form_state Form state.
 * @param $nid The questionnaire node to embargo.
 * @return A confirmation form to embargo $nid.
 */
function cq_questionnaire_embargo_now(&$form_state, $nid) {

  $node = cq_load_questionnaire($nid);

  $form['nid'] = array(
      '#type' => 'value',
      '#value' => $nid,
  );


  $question = t('Do you want to embargo "%qr" now?', array('%qr' => $node->title));
  if ($node->cq_pubdate != NULL) {
    $description = t('Publication will take place on !pubdate',
                    array('!pubdate' => format_date($node->cq_pubdate))
    );
  }
  else {
    $description = t('Publication is not scheduled.<br/>')
            . t('To schedule publication, <a href="/node/!nid/edit">Edit the questionnaire</a>',
                    array('!nid' => $node->nid));
  }


  return confirm_form($form, $question, "node/$nid/edit", $description,
          t('Embargo'), t('Cancel'), 'embargonow');
}

/**
 * Submit handler for cq_questionnaire_embargo_now().
 * @param $form Form data.
 * @param $form_state Form state.
 */
function cq_questionnaire_embargo_now_submit($form, &$form_state) {

  $node = cq_load_questionnaire($form_state['values']['nid']);

  $node->cq_state = CQ_EMBARGOED;
  //Record of publication time?
  node_save($node);

  drupal_set_message(t('The questionnaire has been embargoed.'));

  $form_state['redirect'] = "node/{$node->nid}";
}

/* Publish now: Prevent new/updated responses from candidates, expose all
 * responses to public. */

/**
 * Generate form to allow immediate publication of questionnaires.
 * @param $form_state Form state.
 * @param $nid The node to immediately publish.
 * @return A confirmation form to allow immediate publication.
 */
function cq_questionnaire_publish_now(&$form_state, $nid) {

  //URL-specified nid is a node?
  $node = cq_load_questionnaire($nid);

  $form['nid'] = array(
      '#type' => 'value',
      '#value' => $nid,
  );

  $pubdate = format_date($node->cq_pubdate);

  $question = t('Do you want to publish "%qr" now?', array('%qr' => $node->title));
  if ($node->cq_pubdate != NULL) {
    $description = t('Publication was to take place on !pubdate.',
                    array('!pubdate' => $pubdate)
    );
  }
  else {
    $description = t('Publication was not previously scheduled.<br/>')
            . t('To schedule publication, <a href="/node/!nid/edit">Edit the questionnaire</a>.',
                    array('!nid' => $node->nid));
  }

  return confirm_form($form, $question, "node/$nid/edit", $description,
          t('Publish'), t('Cancel'), 'publishnow');
}

/**
 * Submit handler for cq_questionnaire_publish_now().
 * @param $form Form data.
 * @param $form_state Form state.
 */
function cq_questionnaire_publish_now_submit($form, &$form_state) {

  $node = cq_load_questionnaire($form_state['values']['nid']);

  $node->cq_state = CQ_PUBLISHED;
  //Record of publication time.
  $node->cq_published = time();
  node_save($node);

  drupal_set_message(t('The questionnaire has been published.'));

  $form_state['redirect'] = "node/{$node->nid}";
}

/* Unpublish now: Responses no longer visible to public. */

/**
 * Generates confirmation form to allow immediate un-publication.
 * @param $form_state Form state.
 * @param $nid The node to un-publish.
 * @return A confirmation form for immediate un-publication.
 */
function cq_questionnaire_unpublish_now(&$form_state, $nid) {

  $node = cq_load_questionnaire($nid);

  if ($node->cq_published == NULL) {
    drupal_set_message(t('The specified questionnaire was not published.'), 'error');
    drupal_goto("node/{$node->nid}");
  }

  $form['nid'] = array(
      '#type' => 'value',
      '#value' => $nid,
  );

  $published = format_date($node->cq_published);

  $question = t('Do you want to <em>un-publish</em> "%qr" now?', array('%qr' => $node->title));
  $description = t('Publication took place on !published.<br/>',
                  array('!published' => $published));

  if ($node->cq_pubdate != NULL) {
    $description .= t('Publication was originally scheduled for !date.',
                    array('!date' => format_date($node->cq_pubdate))
    );
  }
  else {
    $description .= t('Publication was not previously scheduled.');
  }


  return confirm_form($form, $question, "node/{$nid}/edit", $description,
          t('Un-publish'), t('Cancel'), 'unpublishnow');
}

/**
 * Submit handler for cq_questionnaire_unpublish_now().
 * @param $form Form data.
 * @param $form_state Form state.
 */
function cq_questionnaire_unpublish_now_submit($form, &$form_state) {

  $node = cq_load_questionnaire($form_state['values']['nid']);

  $node->cq_state = CQ_NONE;
  //Record of publication time.
  $node->cq_published = NULL;
  $node->cq_pubdate = NULL;
  node_save($node);

  drupal_set_message(t('The questionnaire has been un-published.'));

  $form_state['redirect'] = "node/{$node->nid}";
}

/**
 * Lists all questionnaires.
 * @global $user The current user.
 * @return A page showing all embargoed and published questionnaires.
 */
function cq_questionnaire_all() {

  global $user;
  $admin = user_access('create questionnaires', $user);
  $cand = user_access('respond to questionnaires', $user);

  drupal_set_title(t('Questionnaires'));

  //Quick load of all questionnaire nids.
  $type = node_get_types('type', 'cq_questionnaire');
  $results = db_query('SELECT nid FROM {node} WHERE type = "%s"', $type->type);

  $count = 0;
  $qrs = array();
  $rows = array();
  while ($result = db_result($results)) {
    $qrs[$result] = node_load($result); //We know these are all questionnaire nids.
    $state = $qrs[$result]->cq_state;
    $own = ($user->uid == $qrs[$result]->uid);

    switch ($state) {
      case CQ_PUBLISHED:
        $statelabel = t('Published');
        break;
      case CQ_EMBARGOED:
        if (!($admin || $cand))
          continue;
        $statelabel = t('Accepting responses');
        break;
      case CQ_NONE:
        if (!($admin)) //see only own questionnaires?
          continue;
        $statelabel = t('Not accepting responses');
    }

    unset($row);

    $title = $qrs[$result]->title;
    $nid = $qrs[$result]->nid;
    $row[] = l($title, "node/{$nid}");

    $uid = $qrs[$result]->uid;
    $mod = user_load($uid);
    $row[] = l($mod->name, "user/{$uid}");

    $pubdate = $qrs[$result]->cq_pubdate;
    $pubdate = is_null($pubdate) ? t('None') : format_date($pubdate);
    $row[] = $pubdate;

    if ($admin) {
      $row[] = $statelabel;
    }

    $rows[] = $row;
    $count++;
  }

  if (!count($qrs)) {
    $noluck = array(
        '#value' => t('No questionnaires created.'),
    );

    return theme('item', $noluck);
  }

  $header[] = t('Title');
  $header[] = t('Moderator');
  $header[] = t('Publication date');
  if ($admin) {
    $header[] = t('State');
  }

  $info = array(
      '#title' => t('Introduction'),
      //'#description' => t('For more information about these questionnaires,.'),
      '#value' => t('These are candidate questionnaires, composed of series of questions. ' .
              'Candidates respond to questionnaires until publication date, at which time the results are published.')
  );

  return theme('item', $info) . theme('table', $header, $rows, array('width' => '100%'));
}