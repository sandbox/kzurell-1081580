<?php

// $Id$

/**
 * @file
 * Theming for Candidate Questionnaire
 *
 */
/* THEMING */

/**
 * Implements hook_theme() for Candidate Questionnaire.
 *
 * @return An array of theme functions and argument defaults.
 */
function cq_theme() {
  return array(
      'cq_question_question_form' => array(
          'arguments' => array('form' => NULL),
      ),
      'cq_question' => array(
          'arguments' => array('qnode' => NULL),
      ),
      'cq_question_teaser' => array(
          'arguments' => array('qnode' => NULL),
      ),
      'cq_question_item' => array(
          'arguments' => array('qnode' => NULL),
      ),
      'cq_response' => array(
          'arguments' => array('node' => NULL, 'user' => NULL),
      ),
      'cq_rawnotice' => array(
          'arguments' => array('text' => NULL),
      ),
      'cq_qna' => array(
          'arguments' => array('qnode' => NULL, 'rnode' => NULL),
      ),
      'cq_qna_header' => array(
          'arguments' => array('qrnode' => NULL),
      ),
      'cq_user' => array(
          'arguments' => array('user' => NULL),
      ),
      'cq_user_row' => array(
          'arguments' => array('user' => NULL, 'odd' => TRUE),
      ),
      'cq_qr_row' => array(
          'arguments' => array('qr' => NULL, 'odd' => TRUE),
      ),
      'cq_link' => array(
          'arguments' => array('link' => NULL),
      ),
      'cq_qrlink' => array(
          'arguments' => array('qrlink' => NULL),
      ),
      'cq_userlink' => array(
          'arguments' => array('userlink' => NULL),
      ),
      'cq_synopsis' => array(
          'arguments' => array(
              'node' => NULL,
              'qrnode' => NULL,
              'qnode' => NULL,
          )
      ),
      'cq_misc' => array(
          'arguments' => array(
              'content' => NULL,
              'class' => NULL,
          )
      ),
      'cq_body_wrap' => array(
          'arguments' => array(
              'towrap' => NULL,
              'h2' => NULL,
              'heading' => NULL,
          ),
      ),
      'cq_block' => array(
          'arguments' => array(
              'recent' => NULL,
              'upcoming' => NULL, //array()?
          )
      ),
  );
}

function theme_cq_questionnaire_body($qrnode = NULL) {
  return check_markup($qrnode->body);
}

function theme_cq_question_question_form($form) {
  $output = '';

  //KLUDGE
  $output .= drupal_render($form['qrbody']);

  drupal_add_tabledrag('draggable-table', 'order', 'sibling', 'weight-group');
  $header = array('Title', 'Links', 'Weight');
  $rows = array();
  foreach (element_children($form['items']) as $key) {
    $element = &$form['items'][$key];
    $element['weight']['#attributes']['class'] = 'weight-group';
    $row = array();
    $row[] = drupal_render($element['title']);
    $row[] = l('Edit', 'node/' . $element['nid']['#value'] . '/edit') . ' | ' .
            l('Delete', 'node/' . $element['nid']['#value'] . '/delete');
    $row[] = drupal_render($element['weight']) . drupal_render($element['nid']);
    $rows[] = array('data' => $row, 'class' => 'draggable');
  }
  if (count($rows)) {
    $qlist = array(
        '#type' => 'item',
        '#title' => t('Questions'),
        '#value' => theme('table', $header, $rows, array('id' => 'draggable-table', 'width' => '100%')),
        '#description' => t('Drag to reorder, and then press "Save Changes" below to record'),
    );
    $output .= theme('item', $qlist);
  }
  else {
    $notice = array(
        '#type' => 'item',
        '#value' => t('There are no questions in this questionnaire.'),
    );
    $output .= theme('item', $notice);
  }
  $output .= drupal_render($form);
  return $output;
}

function theme_cq_question($qnode) {
  $output[] = '<div class="cq-question">';
  $output[] = "<h2>Question {$qnode->cq_sid}:</h2>";
  $output[] = '<p>' . check_markup($qnode->body) . '</p>';
  $output[] = '</div>';

  return implode('', $output);
}

function theme_cq_question_teaser($qnode) {
  $output[] = '<div class="cq-question-teaser">';
  $output[] = "<h3>Question {$qnode->cq_sid}:</h3>";
  $output[] = '<p>' . check_markup($qnode->body) . '</p>';
  $output[] = '</div>';

  return implode('', $output);
}

function theme_cq_question_item($qnode = NULL) {
  return '<p>' . $qnode->cq_sid . ') ' . check_plain($qnode->body) . '</p>';
}

function theme_cq_response($node, $user) {
  $nlink = theme('username', $user); //l($user->name, "user/{$user->uid}");
  $mlink = l(t('Read more...'), "node/{$node->nid}");

  $output[] = '<div class="cq-response">';
  $output[] = '<h3>' . t('Response by !name', array('!name' => $nlink)) . '</h3>';
  $output[] = '<p>' . check_markup($node->body) . '</p>';
  $output[] = '<p>' . $mlink . '</p>';
  $output[] = '</div>';

  return implode('', $output);
}

function theme_cq_rawnotice($text = NULL) {
  //Duplicate removal?
  return '<p class="cq-notice">' . $text . '</p>';
}

function theme_cq_qna($qnode = NULL, $rnode = NULL, $odd = FALSE) {

  $odd = $odd ? 'odd' : 'even';

  $qlink = l('Read other responses', "node/{$qnode->nid}");
  $rlink = property_exists($rnode, 'nid') ? l('Read more', "node/{$rnode->nid}") : t('(No response)');

  $work[] = "<div class='cq-qna-{$odd}'>";
  $work[] = '<h2>' . t('Question !q', array('!q' => $qnode->cq_sid)) . '</h2>';
  $work[] = '<div class="cq-qna-question">';
  $work[] = check_markup($qnode->body);
  $work[] = '<div class="cq-qna-response">';
  $work[] = check_markup($rnode->body);
  $work[] = '</div>';
  $work[] = '</div>';
  $work[] = '<div class="cq-qna-more">' . $rlink . ' | ' . $qlink . '</div>';
  $work[] = '</div>';

  return implode('', $work);
}

function theme_cq_qna_header($qrnode = NULL) {
  $work[] = '<div class="cq-qna-header">';
  $work[] = '<p>' . check_plain($qrnode->title) . '</p>';
  $work[] = '<p>' . check_markup($qrnode->body) . '</p>';
  $work[] = '</div>';

  return implode('', $work);
}

function theme_cq_user($user = NULL) {
  $work[] = '<div class="cq-user">';
  $work[] = theme('username', $user);
  $work[] = theme('user_picture', $user);
  $work[] = '</div>';

  return implode('', $work);
}

function theme_cq_user_row($user = NULL, $odd = TRUE) {
  $odd = $odd ? 'odd' : 'even';
  $output[] = "<div class='cq-user-row-{$odd}'>";
  $output[] = theme('username', $user);
  $output[] = theme('user_picture', $user);
  $output[] = '</div>';

  return implode('', $output);
}

function theme_cq_qr_row($qr = NULL, $odd = TRUE) {
  $odd = $odd ? 'odd' : 'even';
  $mlink = l($qr->title, "node/{$qr->nid}");
  $output[] = "<div class='cq-qr-row-{$odd}'>";
  $output[] = '<p>' . $mlink . '</p>';
  $output[] = '</div>';

  return implode('', $output);
}

function theme_cq_link($link = NULL) {
  return '<p class="cq-link">' . $link . '</p>';
}

function theme_cq_qrlink($qrlink = NULL) {
  return '<p class="cq-qrlink">' . $qrlink . '</p>';
}

function theme_cq_userlink($userlink = NULL) {
  return '<p class="cq-userlink">' . $userlink . '</p>';
}

function theme_cq_synopsis($node = NULL, $qrnode = NULL, $qnode = NULL) {
  return '<p class="cq_synopsis">' . t('<a href="/user/!uid">@name</a> responded to questionnaire <a href="/node/!qrnid">"@qrname"</a>, <a href="/node/!qnid">Question !sid:</a>',
          array('!uid' => $node->uid, '!qrnid' => $qrnode->nid, '@qrname' => $qrnode->title, '!qnid' => $qnode->nid, '@name' => $node->name, '!sid' => $qnode->cq_sid)) . '</p>';
}

function theme_cq_misc($content = NULL, $class = NULL) {

  if (!is_null($class))
    $class = " class='{$class}'";

  return "<p{$class}>" . $content . "</p>";
}

function theme_cq_body_wrap($towrap = NULL, $h2 = NULL, $heading = NULL) {
  $ho = $h2 ? '<h2>' : '<h3>';
  $hc = $h2 ? '</h2>' : '</h3>';
  $heading = $heading ? $ho . $heading . $hc : '';
  return '<div class="cq-body">'
  . $heading
  . $towrap
  . '</div>';
}

/* Block */

/**
 * Theming for the Candidate Questionnaire block.
 * @global $user The current user.
 * @param $recent Recently-published nodes.
 * @param $upcoming Nodes to be published soon.
 * @return Block HTML.
 */
function theme_cq_block($recent = NULL, $upcoming = NULL) {

  global $user;

  //recents
  $rh = '<h3>' . t('Recently Published') . '</h3>';
  $r = array();
  foreach ($recent as $nid => $node) {
    $l = l($node->title, "node/{$node->nid}");
    $t = (time() - $node->cq_published);
    $t = format_interval($t); //, 2, user_preferred_language($user));
    $r[] = '<p>' . $l . '<br/>' . t('Published @t ago', array('@t' => $t)) . '</p>';
  }
  if (count($r))
    array_unshift($r, $rh);
  else
    $r[] = "<p>" . t('No recently published questionnaires.') . '</p>';


  //upcoming
  $uh = '<h3>' . t('Upcoming') . '</h3>';
  $u = array();
  foreach ($upcoming as $nid => $node) {
    $l = l($node->title, "node/{$node->nid}");
    $t = ($node->cq_pubdate - time());
    $t = format_interval($t); //, 2, user_preferred_language($user));
    $u[] = '<p>' . $l . '<br/>' . t('To be published in @t', array('@t' => $t)) . '</p>';
  }
  if (count($u))
    array_unshift($u, $uh);
  else
    $u[] = "<p>" . t('No upcoming questionnaires.') . '</p>';




  $output = array();
  $output[] = "<div class='cq-block'>";
  $output[] = implode('', $r);
  $output[] = implode('', $u);
  $output[] = "</div>";

  return implode('', $output);
}