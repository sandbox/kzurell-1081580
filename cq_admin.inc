<?php

// $Id$

/**
 * @file
 * Administration facilities for Candidate Questionnaires.
 */

/**
 * Implementation of hook_admin(). Creates a form for module-specific
 * administration settings.
 * @return A system_settings_form().
 */
function cq_admin() {

  $form = array();

  //Set the overall title, for site-wide aggregate pages.
  $form['cq_title'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('Candidate Questionnaire Title'),
      '#default_value' => variable_get('cq_title', ''),
      '#description' => t("Local title for candidate questionnaires."),
  );

  $roles = array();
  $results = db_query('SELECT * FROM {role}');
  while ($result = db_fetch_array($results))
    $roles[$result['rid']] = check_plain($result['name']);

  $form['cq_candidaterole'] = array(
      '#type' => 'select',
      '#options' => $roles,
      '#required' => TRUE,
      '#title' => t('Candidate role'),
      '#default_value' => variable_get('cq_candidaterole', CQ_DEFAULTROLE),
      '#description' => t("Drupal role for candidate users."),
  );

  $form['cq_default_terms'] = array(
      '#type' => 'textarea',
      '#title' => t('Default candidate nomination terms'),
      '#default_value' => variable_get('cq_default_terms', ''),
      '#description' => t('This is the default version of the terms of service candidates must accept in order to respond to a questionnaire. Questionnaire creators can override these terms.'),
  );

  return system_settings_form($form);
}
