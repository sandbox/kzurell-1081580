<?php

// $Id$

/**
 * @file
 * User-oriented functions for Candidate Questionnaire.
 */

/**
 * Implementation of hook_user() for candidate questionnaire.
 * @param $op User operation to be performed.
 * @param $edit Form data.
 * @param $account User object to be inspected.
 * @param $category One of the set previously determined by $op == 'categories'.
 */
function cq_user($op, &$edit, &$account, $category = NULL) {

  global $user;
  $self = @$user->uid == @$account->uid;

  switch ($op) {
    case 'categories':
      break;



    case 'form':
      break;  //return $form
    case 'register':
      break;

    case 'validate':
      break;
    case 'submit':
      //Check for pre-existing nominations.
      $components = split('[[:punct:][:space:]]+', $account->name);
      $namepattern = join("%", $components);
      //Find pre-existing anonymous nominations, update with new user information.
      db_query('UPDATE {cq_nominations} SET uid = %d where uid IS NULL AND (email = "%s" OR name LIKE "%s")',
              $account->uid, $account->mail, $namepattern);
      if (db_affected_rows ()) {
        $link = t('You have been nominated to respond to candidate questionnaires. <a href="/user/!uid">View your profile</a>',
                        array('!uid' => $account->uid));
        drupal_set_message($link);
      }
      break;


    case 'login':
      $any = db_result(db_query('SELECT COUNT(cqn.nid) FROM {cq_nominations} cqn LEFT JOIN {cq_questionnaires} cqr ON cqn.nid = cqr.nid WHERE cqr.cq_state <> 0 AND cqn.uid = %d AND cqn.email_sent = 0', $account->uid));
      if ($any) {
        $qr = format_plural($any, 'a questionnaire', '@count questionnaires');
        $ul = l('See your profile.', "user/{$account->uid}");
        drupal_set_message(t('You have been nominated for !qr. !ul', array("!qr" => $qr, '!ul' => $ul)));
      }
      //Notice?
      break;
    case 'logout':
      break;


    case 'load':
      //$account->
      break;

    case 'view':


      if (user_access('respond to questionnaires', $account)) {
        $account->content['cqsummary'] = _cq_user_respondent($account);
      }


      if (user_access('create questionnaires', $account)) {
        $account->content['cqrsummary'] = _cq_user_admin($account);
      }


      break;


    case 'insert':
      break; //$account-> to database, $edit[] to NULL
    case 'update':
      break;
    case 'after_update':
      break;
    case 'delete':
      //Remove nominations, responses.
      db_query('DELETE FROM {cq_nominations} WHERE uid = %d', $account->uid);
      break;

    default:
      break;
  }
}

// Support function.

/**
 * Helper function that generates a list of questionnaires under administration.
 * @global $user The current user.
 * @param $account The account in question, may not be current user.
 * @return A Drupal content 'item' that lists questionnaires the user administers.
 */
function _cq_user_admin($account) {
  global $user;

  $results = db_query('SELECT * FROM {cq_questionnaires} WHERE uid = %d', $account->uid);
  $rows = array();
  while ($result = db_result($results)) {
    $qr = node_load($result);

    $qcnt = db_result(db_query('SELECT COUNT(nid) FROM {cq_questions} WHERE cq_qrid = %d', $qr->nid));

    switch ($qr->cq_state) {
      case CQ_NONE: $s = t('Not open');
        break;
      case CQ_EMBARGOED: $s = t('Open');
        break;
      case CQ_PUBLISHED: $s = t('Published');
        break;
    }


    unset($row);
    $row[] = l($qr->title, "node/{$qr->nid}");
    $row[] = !is_null($qr->cq_pubdate) ? format_date($qr->cq_pubdate) : t('No automatic publication');
    $row[] = $s;
    $row[] = $qcnt;
    $rows[] = $row;
  }

  $count = count($rows);
  if (count($rows)) {
    $headers = array(t('Questionnaire'), t('Publication Date'), t('State'), t("# of Qs."));
    $qrreport = theme('table', $headers, $rows);
  }
  else {
    $qrreport = t('No Questionnaires');
  }

  $il = l('Create a questionnaire', "node/add/cq-questionnaire");
  $intro = theme('cq_misc', t('Questionnaires are lists of questions with options such as automatic publishing. Questionnaires must be opened for responses, and then published. !il or click on a link below to edit an existing questionnaire.', array('!il' => $il)), '');

  return array(
      '#type' => 'item',
      '#title' => t('Questionnaires under administration'),
      '#description' => t('These are questionnaires you administer (!count)', array('!count' => $count)),
      '#value' => $intro . $qrreport,
  );
}

/**
 * Helper function that generates a list of questionnaires the account is nominated to respond to.
 * @global $user
 * @param $account
 * @return A list of questionnaires the account is nominated to respond to.
 */
function _cq_user_respondent($account) {

  // Who am i?
  global $user;
  // For self, advise of all nominations.
  $self = $user->uid == $account->uid;
  $admin = (/* $user->uid == 1 || */ user_access('create questionnaires', $user));
  $public = user_access('view questionnaires', $user) && (!($admin || $self));

  // All nominations, display some.
  $rows = array();
  $results = db_query('SELECT * FROM {cq_nominations} WHERE uid = %d', $account->uid);
  while ($result = db_fetch_array($results)) {
    // Learn about questionnaire.
    $node = node_load($result['nid']);
    if (!$node)
      continue;

    $pubdate = !is_null($node->cq_pubdate) ? format_date($node->cq_pubdate) : t('Open');
    $published = !is_null($node->cq_published) ? format_date($node->cq_published) : t('Not published');

    $ltitle = '"' . $node->title . '"';
    $userresp = l($ltitle, "user/{$account->uid}/responses/{$node->nid}");
    $qrlink = l($ltitle, "node/{$node->nid}");
    $nomlink = l($ltitle, "node/{$node->nid}/self-nominate");

    $ispublished = $node->cq_state == CQ_PUBLISHED;
    $isembargoed = $node->cq_state == CQ_EMBARGOED;
    $isnone = !($ispublished || $isembargoed);

    /*
      admin / respondent / public
      n  y
      e  y        y1           y2
      p  y        y            y
     */

    // Public, unless something is found differently below.
    $link = $userresp;

    // If not embargoed yet, show questions only to admin/moderator.
    if ($isnone) { // Not yet embargoed.
      if (!$admin) {
        continue;  // Hide all traces from non-admin.
      }
      $link = $qrlink; // No responses yet, show questions only.
    }

    // If embargoed, show responses to self or admin,
    // questions only to other candidates & public.
    if ($isembargoed) {

      if ($public) {
        $link = $qrlink;
        if ($result['accepted'] == 0)
          continue; // Not accepted, hide from others.



      }

      if ($self) {
        if ($result['accepted'] == 0) {
          $link = $nomlink;  // User must accept nomination first.
        }
      }
    }


    unset($row);
    $row[/* cq_pubdate */] = $link;
    $row[] = $node->cq_state == CQ_PUBLISHED ? t('Published') : t('Not published');
    $row[] = $pubdate;
    $rows[] = $row;
  }

  if (count($rows)) {
    $header = array(t('Questionnaire'), t('Published?'), t('Publication Date'));
    $cqsummary = theme('table', $header, $rows, array('width' => '100%'));
  }
  else {
    $cqsummary = t('No questionnaires.');
  }

  $intro = theme('cq_misc', t('Questionnaires are lists of questions created by moderators. If a candidate has accepted a nomination, they can respond before the questionnaire is published.'), '');

  $title = $self ? t('Your questionnaires') : t('Questionnaires for @user', array('@user' => $account->name));
  return array(
      '#type' => 'item',
      '#title' => $title,
      '#description' => t('Questionnaires for which you have been nominated'),
      '#value' => $intro . ' ' . $cqsummary,
  );
}

/* Other user-oriented pages. */

/**
 * Generates a list of all candidates and questionnaires.
 * @return a page of all candidates and questionnaires.
 */
function cq_responses_all() {

  drupal_set_title(t('Candidates and Responses'));

  $instructions = array(
      '#value' => t('View responses by questionnaire or candidate.'),
      '#title' => t('Introduction'),
      '#description' => t("%site is not responsible for the content of candidates' responses.",
              array('%site' => check_plain(variable_get('site_name', t('This site'))))),
  );

  // Cache all respondents & their questionnaires.
  $users = array();
  $uids = array();
  $qrs = array();
  $results = db_query('SELECT uid, cq_qrid FROM {cq_responses} ORDER BY uid, cq_qrid ASC, cq_updated'); //limit 3?
  while ($result = db_fetch_array($results)) {

    // For each user:
    $uid = $result['uid'];
    $users[$uid] = NULL;
    $qrid = $result['cq_qrid'];
    $qrs[$qrid] = NULL;
  }

  foreach ($users as $uid => $nothing)
    $users[$uid] = user_load($uid);

  foreach ($qrs as $qrid => $nothing)
    $qrs[$qrid] = cq_load_questionnaire($qrid);

  // Randomize
  shuffle($users);


  // Generate user table.
  $odd = TRUE;
  $count = 0;
  $rows = array();
  foreach ($users as $uid => $userrec) {
    unset($row);
    $row[] = theme('cq_user_row', $userrec, $odd);
    $rows[] = $row;
    $odd = !$odd;
    $count++;
  }
  $usertable = $count ?
          theme('table',
                  array('Candidates'),
                  $rows,
                  array('width' => '100%', 'class' => 'user-table')
          ) : '<p>' . t('No candidates.') . '</p>';

  // Generate questionnaire table.
  $odd = TRUE;
  $count = 0;
  $rows = array();
  foreach ($qrs as $qrid => $qrrec) {

    // For this list, omit all questionnaires not published.
    if ($qrrec->cq_state != CQ_PUBLISHED)
      continue;

    unset($row);
    $row[] = theme('cq_qr_row', $qrrec, $odd);
    $rows[] = $row;
    $odd = !$odd;
    $count++;
  }
  $qrtable = $count ?
          theme('table',
                  array('Questionnaires'),
                  $rows,
                  array('width' => '100%', 'class' => 'qr-table')) : theme('cq_misc', t('No published questionnaires.'), '');


  return theme('item', $instructions) . $usertable . $qrtable;
}

// Present all responses from a certain user to a certain questionnaire
// (opposite of ???)

/**
 * Display all responses to a particular questionnaire for a particular candidate.
 * @global $user The current user.
 * @param $account The account in question, may not be current user.
 * @param $qrid A questionnaire's node id.
 * @return A page displaying a questionnaire, questions and answers.
 */
function cq_user_responses($account = NULL, $qrid = NULL) {

  $content = array();

  //Who am I?
  global $user;
  $admin = user_access('create questionnaires', $user);

  //Load target user.
  $account = user_load($account);

  //Working for myself or someone else?
  $own = $account->uid == $user->uid;

  //What questionnaire?
  $qr = cq_load_questionnaire($qrid);
  $embargoed = $qr->cq_state == CQ_EMBARGOED;
  $published = $qr->cq_state == CQ_PUBLISHED;

  // Set breadcrumb
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = l($account->name, "user/{$account->uid}");
  $breadcrumb[] = l($qr->title, "node/{$qr->nid}");
  drupal_set_breadcrumb($breadcrumb);

  // Display description of page.
  $title = t('@name responds', array('@name' => $account->name));
  drupal_set_title($title);
  $content[] = theme('cq_qna_header', $qr);


  switch ($qr->cq_state) {

    case CQ_NONE:
      if (!($admin)) {
        drupal_set_message(t('Questionnaire is not yet published.'));
        return implode('', $content);
      }
      break;

    case CQ_EMBARGOED:
      if (!($own || $admin)) {
        drupal_set_message(t('Questionnaire is not yet published.'));
        return implode('', $content);
      }

      // If questionnaire is embargoed and user is viewing own responses, encourage editing.
      $edlink = l('Add or edit responses', "node/{$qr->nid}");
      $content[] = theme('cq_misc', t('Candidates: !edlink.', array('!edlink' => $edlink)), '');
      break;

    case CQ_PUBLISHED:
      break;
  }


  // When no response, use pseudo-response object.
  $emptyr = (object) Array('body' => t('(The candidate has submitted no response.)'));

  $odd = TRUE;
  $count = 0;
  $results = db_query('SELECT q.nid AS qnid, r.nid AS rnid ' .
                  'FROM {cq_questions} q LEFT JOIN {cq_responses} r ON r.cq_qid = q.nid AND r.uid = %d ' .
                  'WHERE q.cq_qrid = %d ORDER BY q.cq_sid', $account->uid, $qr->nid);
  while ($result = db_fetch_array($results)) {
    // Load question.
    $qnode = node_load($result['qnid']);

    // Load response.
    if ($result['rnid']) {
      $rnode = node_load($result['rnid']);
      // TODO: What if response is whitespace?
      $count++;
    }
    else {
      $rnode = $emptyr;
    }


    $content[] = theme('cq_qna', $qnode, $rnode, $odd);
    $odd = !$odd;
  }

  if ($count)
    $content[] = theme('cq_misc', format_plural($count, '1 response', '@count responses'), '');
  else
    $content[] = theme('cq_misc', t('No questions appear in this questionnaire as yet'), '');

  return implode('', $content);
}