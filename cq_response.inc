<?php

// $Id$

/**
 * @file
 * Response node type. Responses are nodes with extra information found from
 * the cq_responses table.
 */

/**
 * Implementation of hook_perm() for responses. Not used at present, stub.
 * @return An array of permissions regarding this type.
 */
function cq_response_perm() {
  return array(
  );
}

/**
 * Implementation of hook_access() for response nodes.
 * @param $op Access operation desired.
 * @param $node Node to access.
 * @param $account User wishing to access.
 * @return TRUE or FALSE.
 */
function cq_response_access($op, $node, $account) {

  //TODO: When creating a new response, should we find out if the user is
  //nominated for the enclosing questionnaire?
  //What can respondents do right now?
  $canresp = user_access('respond to questionnaires', $account);


  // Can view at any time.
  if ($op == 'view')
    return user_access("view questionnaires", $account);

  $own = FALSE;
  if (is_object($node)) {
    if (is_int($node->uid)) {
      $own = ($node->uid == $account->uid);
    }
    //What state is the questionnaire in?
    $qrnode = cq_load_questionnaire($node->cq_qrid);
    $embargoed = $qrnode->cq_state == CQ_EMBARGOED;
  }


  switch ($op) {
    case 'create': // Embargoed? Nominated?
      return $canresp; // Should be && $embargoed, but cannot without qrnode->nid.
    case "update": // Embargoed?
      return $own && $embargoed && $canresp;
    case "delete":
      // Should respondents be able to delete?
      return $own && $embargoed && $canresp;
  }

  return FALSE;
}

/**
 * Implementation of hook_insert() for cq_response.
 * @global $user The current user.
 * @param $node Node to insert into cq_responses.
 */
function cq_response_insert($node) {
  global $user;
  db_query("INSERT INTO {cq_responses} (nid, cq_qrid, cq_qid, cq_updated, uid, cq_sid) VALUES (%d, %d, %d, %d, %d, %d)",
          $node->nid, $node->cq_qrid, $node->cq_qid, time(), $user->uid, $node->cq_sid);
}

/**
 * Implementation of hook_update() for cq_response.
 * @param $node Node to update in cq_responses.
 */
function cq_response_update($node) {
  // If this is a new node or we're adding a new revision...
  /* if ($node->revision) {
    candidate_questions_insert($node);
    }
    else { */
  db_query("UPDATE {cq_responses} SET cq_updated = %d WHERE nid = %d",
          time(), $node->nid);
  /* } */
}

/**
 * Implementation of hook_delete() for cq_response.
 * @param $node Node to delete from cq_responses.
 */
function cq_response_delete($node) {
  //Delete node-attached data.
  db_query('DELETE FROM {cq_responses} WHERE nid = %d', $node->nid);
}

/**
 * Implementation of hook_load() for responses.
 * Load additional data for a response node.
 *
 * @param $node
 *   Node to expand with addtl data.
 * @return
 *  An object with properties to integrate into a node object.
 */
function cq_response_load($node) {
  $additions = db_fetch_object(db_query('SELECT * FROM {cq_responses} WHERE nid = %d', $node->nid));
  return $additions;
}

/**
 * Load and verify a cq_response node.
 * @param $nid Node ID of a response node.
 * @return Node object for a cq_response.
 */
function cq_load_response($nid = NULL) {

  $node = node_load($nid);

  if (!$node) {
    drupal_set_message(t('Internal error: the specified URL is invalid (response).'), 'error', FALSE);
    drupal_goto('<front>');
  }

  $type = node_get_types('type', 'cq_response');

  if (!($type->type == $node->type)) {
    drupal_set_message(t('Internal error: the specified URL does not refer to a response.'), 'error', FALSE);
    drupal_goto('<front>');
  }


  return $node;
}

/**
 * Implementation of hook_view() for responses. View a response node.
 *
 * @param $node
 *   Response node to view.
 * @param $teaser
 *   Return just the teaser or the complete node.
 * @param $page
 *   The results will form an independent page, or not.
 * @return
 *   The node with viewable content in $node['content'].
 */
function cq_response_view($node, $teaser = FALSE, $page = FALSE) {

  global $user;

  //Who am I?
  $own = $user->uid == $node->uid;
  $admin = user_access('create questionnaires');
  $respond = user_access('respond to questionnaires');
  $public = !($admin || $respond);

  $preview = !($teaser || $page);

  //Get ready to display.
  $node = node_prepare($node, $teaser);

  $qnode = cq_load_question($node->cq_qid);
  $qrnode = cq_load_questionnaire($node->cq_qrid);



  // Change appearance.
  switch ($qrnode->cq_state) {
    case CQ_NONE:
      break;
    case CQ_EMBARGOED:
      if (!($admin || $own))
        $node->content['body'] = array(
            '#value' => theme('cq_misc', t('This questionnaire has not been published'), 'cq-notice'),
        );
      //unset($node->content['body']);
      break;
    case CQ_PUBLISHED:
      break;
  }


  if ($teaser) {
    // Display question teaser.
    $node->content['question'] = array(
        '#weight' => -5,
        '#value' => theme('cq_question_teaser', $qnode)
    );
    // Give a tantalizing synopsis.
    $node->content['teaser_name'] = array(
        '#weight' => -10,
        '#value' => theme('cq_synopsis', $node, $qrnode, $qnode),
    );
    //KLUDGE KLUDGE KLUDGE
    //Wrap response teaser with something we can style.
    $node->content['body']['#value'] = theme('cq_body_wrap',
                    $node->content['body']['#value'],
                    FALSE/* ==H3 */,
                    t('Response'));

    return $node; // SHORT CIRCUIT
  }



  // Set breadcrumb.
  $breadcrumb[] = l(t('Home'), '<front>');
  $breadcrumb[] = l($qrnode->title, "node/{$qrnode->nid}");
  $qqq = t('Question !sid', array('!sid' => $qnode->cq_sid));
  $breadcrumb[] = l($qqq, "node/{$qnode->nid}");
  drupal_set_breadcrumb($breadcrumb);



  // Display question.
  $node->content['question'] = array(
      '#weight' => -5,
      '#value' => theme('cq_question', $qnode),
  );



  // Link to other responses by same user.
  $userlink = $own ? 'you' : $node->name;
  $userlink = l($userlink, "user/{$node->uid}/responses/{$node->cq_qrid}");
  $userlink = t('See other responses by !uname.', array('!uname' => $userlink));

  $qrlink = l($qrnode->title, "node/{$qrnode->nid}");
  $qrlink = t('See other questions in !qrlink.', array('!qrlink' => $qrlink));

  if (!$preview) {
    // Show only on live page.
    $node->content['qrlink'] = array(
        '#weight' => 9,
        '#value' => theme('cq_qrlink', $qrlink),
    );
    $node->content['userlink'] = array(
        '#weight' => 10,
        '#value' => theme('cq_userlink', $userlink),
    );
  }

  // Alter body to allow div.class for formatting response.
  $node->content['body']['#value'] = theme('cq_body_wrap', $node->content['body']['#value'], TRUE/* ==H2 */, t('Response'));


  return $node;
}

/**
 * Implementation of hook_form() for responses.
 * Display response editing form.
 *
 * This form appears either as:
 * - node/add/cq-response from generic link,
 * - node/add/cq-response/qr/q where qr and q are node IDs
 *   of enclosing questionnaire and question,
 * - node/x/edit where x is an existing response.
 *
 * @param $node
 *   Response node to be edited.
 * @param $form_state
 *   Form state.
 * @return string
 *   Form array for editing response.
 */
function cq_response_form(&$node, $form_state) {

  global $user;

  $name = check_plain($user->name);

  // Ensure we know what questionnaire and question, if any.
  if (property_exists($node, 'cq_qrid')) {
    // Existing response, should have own qr and q.
    $qr = $node->cq_qrid;
    $q = $node->cq_qid;
  }
  else {
    // New response, we should know qr and q from request.
    $qr = arg(3);
    $q = arg(4);
    if (!($qr && $q)) {
      // We don't know qr and q from request, direct user to a different
      // link.
      // Stub.
      drupal_set_message('To respond, select a questionnaire.');
      $u = $user->uid;
      if ($u)
        drupal_goto("user/{$u}");
      else {
        //Stub.
        drupal_goto('elsewhere');
      }
    }
  }


  // Retrieve relevant questionnaire and question,
  // verify they're useful

  $qrn = cq_load_questionnaire($qr);
  $qn = cq_load_question($q);

  // Set up form
  $qbody = check_plain($qn->body);

  $type = node_get_types('type', $node);
  if ($type->has_title) {
    $form['title'] = array(
        '#type' => 'value',
        // Title is predefined.
        '#value' => t('@name responds', array('@name' => $user->name)),
    );
  }

  if ($type->has_body) {
    $form['body_field'] = node_body_field($node, /* $type->body_label */ t('Your response'), $type->min_word_count);
  }


  // Form elements specific to our node type.
  $form['question'] = array(
      '#value' => theme('cq_question', $qn), // Display question as markup.
      '#weight' => -10,
  );

  $form['cq_qid'] = array(
      '#type' => 'value',
      '#value' => $qn->nid,
  );
  $form['cq_sid'] = array(
      '#type' => 'value',
      '#value' => $qn->cq_sid,
  );
  $form['cq_qrid'] = array(
      '#type' => 'value',
      '#value' => $qrn->nid,
  );

  return $form;
}

/*
 * Remove unneeded stuff from response form.
 * As we control many things about responses,
 * get rid of them.
 */
//function cq_response_form_alter(&$form, &$form_state, $form_id) {
//  $temp = $form_id;
//}

